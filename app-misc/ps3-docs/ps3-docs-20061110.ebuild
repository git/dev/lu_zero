# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Documentation about Sony Playstation 3 linux programming"
HOMEPAGE=""
SRC_URI="http://67.117.136.164/pub/cell/linux-${PV}-docs.tar.bz2"

LICENSE="FDL-1.2"
SLOT="0"
KEYWORDS="~ppc"
IUSE=""

DEPEND=""
RDEPEND=""

src_compile() {
	einfo "nothing to do"
}

src_install() {
	cd "${WORKDIR}/linux-${PV}-docs/"
	insinto /usr/share/doc/${PF}
	doins -r .
}
