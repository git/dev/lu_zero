# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
inherit libtool autotools

DESCRIPTION="Cell SPE examples"
HOMEPAGE=""
SRC_URI="http://dev.gentoo.org/~lu_zero/distfiles/spe-samples-1.0.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~ppc ~ppc64"
IUSE=""

DEPEND=">=sys-libs/libspe2-2.0
		>=sys-libs/libspe-1.2.0"
RDEPEND=""

# fixme: provide an spu eclass to do basic checks and autostuff fixes

src_unpack() {
	unpack ${A}
	# make sure it uses the right compiler.
	sed -i -e "s:spu-gcc:spu-elf-gcc:g" \
		-e "s:ppu-::" ${S}/*/spe/configure*
	# avoid wrong binary
	sed -i -e "s:-m32::" ${S}/*/spe/Makefile.am
	# fixme: kill c++/fortran checks!
	cd ${S}
	eautoreconf
}

src_compile() {
	# just to be sure
	unset CFLAGS
	unset CXXFLAGS
	unset CFLAGS_ppc64
	unset CFLAGS_ppc
	unset LDFLAGS_ppc64
	unset LDFLAGS_ppc
	./configure --prefix=/usr || die "configure failed"
	emake || die "build failed"
}

src_install() {
	make DESTDIR=${D} install || die "install failed"
}
