# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-kernel/gentoo-sources/gentoo-sources-2.6.16-r13.ebuild,v 1.3 2006/07/24 14:14:55 gustavoz Exp $

ETYPE="sources"
K_WANT_GENPATCHES="base"
K_GENPATCHES_VER="15"
IUSE=""
inherit kernel-2 eutils
detect_version
detect_arch

KEYWORDS="~ppc ~ppc64"
HOMEPAGE="http://dev.gentoo.org/~dsd/genpatches
http://www.bsc.es/projects/deepcomputing/linuxoncell/stable/linuxkernel.html"

DESCRIPTION="Full sources including the gentoo patchset for the ${KV_MAJOR}.${KV_MINOR} kernel tree"

BSC_URI="http://www.bsc.es/projects/deepcomputing/linuxoncell/stable/patches/"

CELLPATCHES_URI="
${BSC_URI}hvc-console-rework-4.diff
${BSC_URI}hvc-console-rtas-4.diff
${BSC_URI}cell-detect.diff
${BSC_URI}iommu-fix.diff
${BSC_URI}cell-defconfigs-11.diff
${BSC_URI}cell-pic-updates-3.diff
${BSC_URI}powerpc-asm-syscalls-h-2.diff
${BSC_URI}spufs-callbacks-3.diff
${BSC_URI}unshare-decl.diff
${BSC_URI}spufs-mfc-file.diff
${BSC_URI}spufs-ps-mapping-3.diff 
${BSC_URI}spufs-ini-section.diff
${BSC_URI}init_mfc.diff
${BSC_URI}spufs-ls-protfault-fix.diff
${BSC_URI}spidernet-buildfix.diff
${BSC_URI}spidernet-selectfwloader.diff
${BSC_URI}spidernet-txcsum-2.diff
${BSC_URI}spidernet-rxramfl.diff
${BSC_URI}iic-map-guarded.diff
${BSC_URI}spider-pic-nodeid.diff
${BSC_URI}defconfig-update.diff
${BSC_URI}spufs-decrementer-fix.diff
${BSC_URI}hvc-console-fast.diff
${BSC_URI}spu-base-no-module-2.diff
${BSC_URI}spidernet-bcm5461-2.diff
${BSC_URI}ib-disable_tune_pci.diff
${BSC_URI}ib-ioremap-3.diff
${BSC_URI}ib-mthca_reset.diff
${BSC_URI}systemsim-base.diff
${BSC_URI}systemsim-block.diff
${BSC_URI}systemsim-bd-fixup.diff
${BSC_URI}systemsim-net.diff
${BSC_URI}systemsim-defconfig.diff
${BSC_URI}hvc-console-fss-2.diff
${BSC_URI}mss-map.diff
${BSC_URI}systemsim-idle.diff
${BSC_URI}systemsim_idlefix.diff
${BSC_URI}cell-iic-cleanup.diff
${BSC_URI}spidernet-gbeburst.diff
${BSC_URI}spidernet-tx-queue-rework.diff
${BSC_URI}dd2-performance.diff
${BSC_URI}dd2-hack-runlatch-hack.diff
${BSC_URI}pci-fixup-hack.diff
${BSC_URI}parm-fixup.diff
${BSC_URI}spufs-sparsemem-extreme-2.diff
${BSC_URI}memory-add.diff
${BSC_URI}spu-hash-page-fix-2.diff
${BSC_URI}cell-cross-build-2.diff
${BSC_URI}cbesim-defconfig-2.diff
${BSC_URI}spufs-rmdir-3.diff
${BSC_URI}spidernet-rxramfull-fix.diff
${BSC_URI}64-k-page-cell-3.diff
${BSC_URI}64k-page-enable.diff
${BSC_URI}fix-tlbie-64k-page.diff
${BSC_URI}spufs-64-k-fix.diff
${BSC_URI}64k-page-exports.diff
${BSC_URI}spufs-smm-hid.diff
${BSC_URI}spufs-64k-csa.diff
${BSC_URI}defconfig-numa.diff
${BSC_URI}defconfig-tun.diff
${BSC_URI}defconfig-bonding.diff
${BSC_URI}spufs-phys-id.diff
${BSC_URI}spufs-fixme.diff
${BSC_URI}spufs-initial-wbox-stat.diff
${BSC_URI}spufs-kzalloc.diff
${BSC_URI}spufs-node-to-nid-2.diff
${BSC_URI}fix-spus-stuck-in-nid-0.diff
${BSC_URI}spufs-numa-id.diff
${BSC_URI}spufs-register-sysdev.diff
${BSC_URI}spufs-ctx-kzalloc.diff
${BSC_URI}spufs-channel-1-count.diff
${BSC_URI}cell-ras-3.diff
${BSC_URI}spufs-sched-numa-2.diff
${BSC_URI}cell-hvc-fss-detection.diff
${BSC_URI}cell-defconfigs-oprofile.diff
${BSC_URI}cell-perfmon.diff
${BSC_URI}cell-perfmon-fix.diff
${BSC_URI}cell-perfmon-more-fixes.diff
${BSC_URI}cell-perfmon-cleanup.diff
${BSC_URI}cell-oprofile.diff
${BSC_URI}cell-oprofile-disable.diff
${BSC_URI}cell-oprofile-2.6.16.diff
${BSC_URI}alp-remove-null-setup-cpu.patch
${BSC_URI}alp-split-platform-code.patch
${BSC_URI}alp-spufs-multi-platform.patch
${BSC_URI}alp-wrap-cpu-affinity.patch
${BSC_URI}spufs-fix-remove-stop_code-member-of-struct-spu.diff
${BSC_URI}spufs-fix-clean-_dump.h.diff
${BSC_URI}spufs-fix-class2-clear-stat-before-wakeup.diff
${BSC_URI}fix-null-pgsz-get-pointer.diff
${BSC_URI}spufs-check-flags.diff
${BSC_URI}spufs-fix-context-switch-during-fault.diff
${BSC_URI}spufs-dma-status.diff
${BSC_URI}spufs-map-guarded.diff
${BSC_URI}spufs-dma-events-2.diff
${BSC_URI}spufs-correct-dma-exceptions.diff"

SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI} ${CELLPATCHES_URI}"

src_unpack() {
	kernel-2_src_unpack

	cd ${S}

	einfo "Applying Cell patchset"
	for pt in ${CELLPATCHES_URI}
	do
		patch -p1 -g0 -E --no-backup-if-mismatch \
			< "${DISTDIR}/${pt/${BSC_URI}}" \
			&> /dev/null || die "${pt/${BSC_URI}} failed"
	done
}

pkg_postinst() {
	postinst_sources

	echo

	if [ "${ARCH}" = "sparc" ]; then
		if [ x"`cat /proc/openprom/name 2>/dev/null`" \
			 = x"'SUNW,Ultra-1'" ]; then
			einfo "For users with an Enterprise model Ultra 1 using the HME"
			einfo "network interface, please emerge the kernel using the"
			einfo "following command: USE=ultra1 emerge ${PN}"
		fi
	fi
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
}
