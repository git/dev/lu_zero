# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-devel/gcc-nios2/gcc-nios2-5.1.ebuild,v 1.3 2006/04/11 00:17:38 vapier Exp $

GCC_A_FAKEIT="toolchain-3.2-src.tar.bz2"

ETYPE="gcc-compiler"

#SPLIT_SPECS=false
TOOLCHAIN_GCC_PV=4.0.2
#hack hack hack
GCC_FILESDIR="${PORTDIR}/sys-devel/gcc/files"

inherit toolchain eutils

BSC_URI="http://www.bsc.es/projects/deepcomputing/linuxoncell/stable/toolchain"

DESCRIPTION="Compiler for Cell SPU and PPU targets"
SRC_URI="${BSC_URI}/toolchain-3.2-src.tar.bz2
		 ${BSC_URI}/ppu/toolchain-altivec-fix.diff
		 ${BSC_URI}/ppu/toolchain-convertible-revert.diff
		 ${BSC_URI}/ppu/toolchain-libiberty-merge.diff
		 ${BSC_URI}/ppu/toolchain-altivec-cmple-fix.diff
		 ${BSC_URI}/ppu/toolchain-m32-m64-multilib-only.diff
		 ${BSC_URI}/spu/toolchain-build-revert.diff
		 ${BSC_URI}/spu/toolchain-mfcio-stdint.diff"

KEYWORDS="-* ~amd64 ~ppc ~ppc64 ~x86"

DEPEND=">=${CATEGORY}/binutils-2.17-r2
		>=sys-devel/patch-2.5.9-r1"

S=${WORKDIR}/toolchain/gcc

src_unpack() {
	local CELL_PATCHES="build-revert convertible-revert libiberty-merge
						altivec-fix altivec-cmple-fix"

	toolchain_src_unpack

	[[ ${CTARGET} == spu* ]] && \
			CELL_PATCHES="${CELL_PATCHES} mfcio-stdint"
	[[ ${CTARGET} == powerpc64* ]] && \
			CELL_PATCHES="${CELL_PATCHES} m32-m64-multilib-only"

	for pt in ${CELL_PATCHES}
	do
		epatch "${DISTDIR}/toolchain-${pt}.diff"
	done
	cd ${WORKDIR}
	epatch ${FILESDIR}/embedspu-gentoo.patch
}

src_install() {
	toolchain_src_install

	if [[ ${CTARGET} == powerpc64* ]]
	then
		into /usr/${CTARGET}
		dobin ${WORKDIR}/toolchain/scripts/embedspu.sh
		cp /usr/${CTARGET}/bin/embedspu.sh \
		${D}/usr/bin/${CTARGET}-embedspu
	fi
}
