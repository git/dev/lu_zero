# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit git autotools

EGIT_REPO_URI="git://git.lscube.org/netembryo.git"
EGIT_BOOTSTRAP="eautoreconf"

DESCRIPTION="Network abstraction library"
HOMEPAGE="http://lscube.org/projects/netembryo"
LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~ppc64"
IUSE="sctp"

RDEPEND="sctp? ( net-misc/lksctp-tools )"
DEPEND="${RDEPEND}"

src_compile() {
	econf $(use_enable sctp) || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc README
	dodoc ChangeLog
}
