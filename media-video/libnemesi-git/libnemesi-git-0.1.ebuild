# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit autotools git eutils

EGIT_REPO_URI="git://git.lscube.org/libnemesi.git"
EGIT_BOOTSTRAP="eautoreconf"

DESCRIPTION="Standard rtsp streaming client library"
HOMEPAGE="http://lscube.org/projects/libnemesi"
LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~ppc64"
IUSE="sctp"

# self contained
RDEPEND="media-video/netembryo-git[sctp?]"
DEPEND="${RDEPEND}"

src_configure() {
	econf $(use_enable sctp)  || die "econf failed"
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc README
	dodoc ChangeLog
}
