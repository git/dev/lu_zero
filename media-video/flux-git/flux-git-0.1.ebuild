# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit git autotools

EGIT_REPO_URI="git://git.lscube.org/flux.git"
EGIT_BOOTSTRAP="eautoreconf"

AT_M4DIR="m4"

DESCRIPTION="RTP streams multiplexer"
HOMEPAGE="http://lscube.org/projects/flux"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~ppc64"
IUSE="ffmpeg"

RDEPEND="ffmpeg? ( media-video/ffmpeg )
		 media-video/netembryo-git"

DEPEND="${RDEPEND}"

src_compile() {
	econf --localstatedir=/var `use_with ffmpeg libavcodec` || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc README
}
