# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit git autotools

EGIT_REPO_URI="git://git.lscube.org/feng.git"
EGIT_BOOTSTRAP="eautoreconf"

AT_M4DIR="m4"

DESCRIPTION="Standard rtsp streaming server"
HOMEPAGE="http://lscube.org/projects/feng"
LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~ppc64"
IUSE="sctp"

RDEPEND="media-video/ffmpeg
		 >=dev-libs/glib-2
		 media-video/netembryo-git[sctp?]
		 dev-libs/libev"

DEPEND="${RDEPEND}
		>=sys-devel/libtool-2
		dev-util/ragel"

pkg_setup() {
	enewgroup feng
	enewuser feng -1 -1 /var/feng feng
}

pkg_config() {
	einfo "Installing default avroot to ${ROOT}/var/feng/avroot"
	mkdir -p "${ROOT}"/var/feng/avroot
	cp -R "${ROOT}"/usr/share/doc/${PF}/avroot/* "${ROOT}"/var/feng/avroot
	chown -R feng:0 "${ROOT}"/var/feng/avroot
}

pkg_postinst() {
	if [[ -e "${ROOT}/var/feng/avroot" ]] ; then
		einfo "The default avroot has not been installed into"
		einfo "${ROOT}/var/feng/avroot because the directory already exists"
		einfo "and we do not want to overwrite any files you have put there."
		einfo
		einfo "If you would like to install the latest avroot, please run"
		einfo "emerge --config =${PF}"
	else
		einfo "Installing default avroot to ${ROOT}/var/feng/avroot"
		mkdir -p "${ROOT}"/var/feng/avroot
		cp -R "${ROOT}"/usr/share/doc/${PF}/avroot/* "${ROOT}"/var/feng/avroot
		chown -R feng:0 "${ROOT}"/var/feng/avroot
	fi
}

src_configure() {
	econf --localstatedir=/var \
		 $(use_enable sctp) || die "econf failed"
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc README
	mv ${D}/var/feng/avroot ${D}/usr/share/doc/${PF}/
	dodoc ChangeLog
	newinitd ${FILESDIR}/feng.rc feng
	newconfd ${FILESDIR}/feng.confd feng
}
