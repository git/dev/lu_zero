# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit rpm

DESCRIPTION="Full-System Simulator for the Cell Broadband Engine Processor"
HOMEPAGE="http://www.alphaworks.ibm.com/tech/cellsystemsim"
SRC_URI="ppc? ( ${P}-10.ppc.rpm )
		 x86? ( ${P}-10.i386.rpm)
		 ppc64? ( ${P}-10.ppc64.rpm )
		 amd64? ( ${P}-10.x86_64.rpm )"

LICENSE="ILAR"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86"
IUSE=""

SYSTEMSIM_DIR="/opt/IBM/systemsim-cell"
IMAGE_PATH="/usr/share/${PN}/image"

DEPEND=">=dev-lang/tcl-8.4.10
		>=dev-lang/tk-8.4.10"
#		>=dev-tcltk/blt-2.4
#		>=dev-tcltk/bwidget-1.7"

RESTRICT="fetch nostrip"

pkg_nofetch() {
	einfo "Please download ${A} yourself from:"
	einfo "http://www.alphaworks.ibm.com/tech/cellsystemsim/download"
	einfo "and place it in ${DISTDIR}"
}

src_install() {
	cp -pPR ${WORKDIR}/opt ${D}
	rm -fR ${D}/${SYSTEMSIM_DIR}/doc
	insinto /usr/share/doc/${PF}/
	doins ${WORKDIR}/${SYSTEMSIM_DIR}/doc/*.pdf
	dohtml -r ${WORKDIR}/${SYSTEMSIM_DIR}/doc/gdb
	doenvd ${FILESDIR}/09systemsim-cell
	sed -i -e "s:\${SYSTEMSIM_TOP}/images:${IMAGE_PATH}:" \
		${D}/${SYSTEMSIM_DIR}/bin/systemsim
	dodir ${IMAGE_PATH}/cell
	echo "Put a system image with the name sysroot_disk and a kernel image with
	the name vmlinux there" > ${D}/${IMAGE_PATH}/cell/README
}

pkg_postinst() {
	elog "The provided systemsim doesn't have kernel and system images, please"
	elog "install them in ${IMAGE_PATH}/cell"
}
